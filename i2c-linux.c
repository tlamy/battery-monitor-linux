//
// Created by Thomas Lamy on 01.02.23.
//
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>

static int i2c_fd;


int open_i2c_device(const char *linux_device, uint8_t i2c_device) {
    i2c_fd = open(linux_device, O_RDWR);
    if (i2c_fd == -1) {
        perror(linux_device);
        exit(1);
    }
    if (ioctl(i2c_fd, I2C_SLAVE, i2c_device) < 0) {
        perror("ioctl failed");
        exit(1);
    }
    return i2c_fd;
}

static inline int i2c_smbus_rdwr_block(int fd, uint8_t reg, uint8_t read_write, uint8_t length, unsigned char *buffer) {
    struct i2c_smbus_ioctl_data ioctl_data;
    union i2c_smbus_data smbus_data;

    int rv;

    if (length > I2C_SMBUS_BLOCK_MAX) {
        fprintf(stderr, "Requested Length is greater than the maximum specified\n");
        return -1;
    }

    // First byte is always the size to write and to receive
    // https://github.com/torvalds/linux/blob/master/drivers/i2c/i2c-core-smbus.c
    // (See i2c_smbus_xfer_emulated CASE:I2C_SMBUS_I2C_BLOCK_DATA)
    smbus_data.block[0] = length;

    if (read_write != I2C_SMBUS_READ) {
        for (int i = 0; i < length; i++) {
            smbus_data.block[i + 1] = buffer[i];
        }
    }


    ioctl_data.read_write = read_write;
    ioctl_data.command = reg;
    ioctl_data.size = I2C_SMBUS_I2C_BLOCK_DATA;
    ioctl_data.data = &smbus_data;

    rv = ioctl(fd, I2C_SMBUS, &ioctl_data);
    if (rv < 0) {
        perror("Accessing I2C Read/Write failed! Error is: ");
        return rv;
    }

    if (read_write == I2C_SMBUS_READ) {
        int i;
        for (i = 0; i < length; i++) {
            // Skip the first byte, which is the length of the rest of the block.
            buffer[i] = smbus_data.block[i + 1];
        }
        //buffer[i] = '\0';
    }

    return smbus_data.block[0];
}

static inline unsigned char *i2c_smbus_read_cstring_impl(int fd, unsigned char reg, unsigned char *buffer, size_t buf_size) {
    unsigned char local_buffer[buf_size];
    if (i2c_smbus_rdwr_block(fd, reg, I2C_SMBUS_READ, buf_size, local_buffer) < 0) return NULL;
    memcpy(buffer, local_buffer + 1, local_buffer[0]);
    buffer[local_buffer[0]] = '\0';
    return buffer;
}

unsigned char *i2c_smbus_read_cstring(unsigned char sm_register, unsigned char *buffer, size_t buf_size) {
    usleep(10000);
    return i2c_smbus_read_cstring_impl(i2c_fd, sm_register, buffer, buf_size);
}
int8_t i2c_smbus_read_word(uint8_t sm_register, uint16_t *value) {
    errno=0;
    *value = i2c_smbus_read_word_data(i2c_fd, sm_register);
    if(*value & 0x8000 && errno != 0) {
        return -1;
    }
}
uint16_t i2c_smbus_write_word(uint8_t sm_register, uint16_t data) {
    usleep(10000);
    return i2c_smbus_write_word_data(i2c_fd, sm_register, data);
}
