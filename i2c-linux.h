//
// Created by Thomas Lamy on 01.02.23.
//

#ifndef BATMON_I2C_LINUX_H
#define BATMON_I2C_LINUX_H

#include <stdint.h>
#include <stddef.h>

extern int open_i2c_device(const char *linux_device, uint8_t i2c_device);

extern int8_t i2c_smbus_read_word(uint8_t sm_register, uint16_t *data);

extern unsigned char *i2c_smbus_read_cstring(unsigned char sm_register, unsigned char *buffer, size_t buf_size);

uint16_t i2c_smbus_write_word(uint8_t sm_register, uint16_t data);

#endif //BATMON_I2C_LINUX_H
