This little program display data from a MacBook Air battery.

It has bugs!

My test setup has 
* Raspi Pin 3 => SDA on battery (Pin 5)
* Raspi Pin 5 => SCL on battery (Pin 4)
* Raspi Pin 6 => GND on battery (Pin 9, 8 or 7)
When in doubt, check with the board view.
### Building

```cmake --build .```

### Running
```./batmon```


### Missing features
* check if a battery is in "Permanent Fail" state
* clear failed state
* maybe reset cycle count, build date etc (no, not really)

