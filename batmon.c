/**
 * batmon.c - A simple battery monitor for batteries driven by TI bq20z40 alike management chips
 * See https://www.ti.com/lit/er/sluu313a/sluu313a.pdf
 */
#include <stdio.h>
#include "i2c-linux.h"
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <arpa/inet.h>

const unsigned char device_no = 0x0b;
#define MANUFACTURER_ACCESS 0x00

struct date {
    uint16_t year;
    uint8_t month;
    uint8_t day;
};


float bat_temp_c() {
    uint16_t data;
    if(i2c_smbus_read_word(0x08, &data) < 0) {
        perror("bat_temp_c (0x08)");
    };
    return data / 10.0 - 273.1;
}

float bat_pack_volts() {
    uint16_t data;
    if(i2c_smbus_read_word(0x09, &data) < 0) {
        perror("bat_pack_volts (0x09)");
    };
    return data / 1000.0;
}

float bat_cell_volts(unsigned char cell) {
    uint16_t data;
    if (cell < 1 || cell > 4) return 0.0;
    if(i2c_smbus_read_word(0x3b + cell, &data) < 0) {
        perror("bat_cell_volts (0x3b)");
    };
    return data / 1000.0;
}

float bat_pack_amps() {
    uint16_t data;
    if(i2c_smbus_read_word(0x0a, &data) < 0) {
        perror("bat_pack_amps (0x0a)");
    };
    return data / 1000.0;
}

float bat_pack_amps_average() {
    uint16_t data;
    if(i2c_smbus_read_word(0x0b, &data) < 0) {
        perror("bat_pack_amps_average (0x0b)");
    };
    return data / 1000.0;
}

float bat_capacity_remaining() {
    uint16_t data;
    if(i2c_smbus_read_word(0x0f, &data) < 0) {
        perror("bat_capacity_remaining (0x0f)");
    };
    return data / 1000.0;
}

float bat_capacity() {
    uint16_t data;
    if(i2c_smbus_read_word(0x10, &data) < 0) {
        perror("bat_capacity (0x10)");
    };
    return data / 1000.0;
}

float bat_design_capacity() {
    uint16_t data;
    if(i2c_smbus_read_word(0x18, &data) < 0) {
        perror("bat_design_capacity (0x18)");
    };
    return data / 1000.0;
}

float bat_design_volts() {
    uint16_t data;
    if(i2c_smbus_read_word(0x19, &data) < 0) {
        perror("bat_design_volts (0x19)");
    };
    return data / 1000.0;
}


uint16_t bat_status() {
    uint16_t data;
    if(i2c_smbus_read_word(0x16, &data) < 0) {
        perror("bat_status (0x16)");
    };
    return data;
}

uint16_t bat_cycle_count() {
    uint16_t data;
    if(i2c_smbus_read_word(0x17, &data) < 0) {
        perror("bat_cycle_count (0x17)");
    };
    return data;
}

unsigned char *bat_manufacturer(unsigned char *buffer) {
    return i2c_smbus_read_cstring(0x20, buffer, 32);
}

unsigned char *bat_devicename(unsigned char *buffer) {
    return i2c_smbus_read_cstring(0x21, buffer, 32);
}

unsigned char *bat_chemistry(unsigned char *buffer) {
    return i2c_smbus_read_cstring(0x22, buffer, 32);
}

void bat_manufactured_date(struct date *date) {
    uint16_t packed_date;
    i2c_smbus_read_word(0x1b, &packed_date);
    date->year = ((packed_date / 512) & 0x00ef) + 1980;
    date->month = ((packed_date & 0x01e0) / 32) & 0x000f;
    date->day = packed_date & 0x001f;
}


uint16_t i2c_smbus_manufacturer_read_word(uint16_t command) {
    uint16_t data = 0xaaaa;
    if (i2c_smbus_write_word(MANUFACTURER_ACCESS, command) < 0) {
        perror("Writ to MANUFACTURER_ACCESS failed");
        return 0;
    };
    if(i2c_smbus_read_word(MANUFACTURER_ACCESS, &data) < 0) {
        perror("MANUFACTURER_ACCESS (0x00)");
    };
    return data;
}

uint8_t is_sealed() {
    uint16_t opStatus = i2c_smbus_manufacturer_read_word(0x54);
    printf("OperationStatus     : 0x%04x  ", opStatus);
    if ((opStatus & 0b0100000000000000) == 0) printf("FAS ");
    if (opStatus & 0b0010000000000000) printf("SEALED ");
    if (opStatus & 0x0020) printf("DIS-FAULT ");
    if (opStatus & 0x0010) printf("DIS-DISABLED ");
    if (opStatus & 0x0008) printf("DIS-INHIBIT ");
    if ((opStatus & 0x0002) == 0) printf("!VOK ");
    if ((opStatus & 0x0001) == 0) printf("!QEN ");
    printf("\n");
    return (opStatus & 0b0010000000000000) != 0;
}

void unseal() {
    // Unseal battery - 36720414
    // @see https://e2e.ti.com/support/power-management-group/power-management/f/power-management-forum/1057725/bq27541-how-to-find-out-that-unseal-key-is-correct
    if(i2c_smbus_write_word(MANUFACTURER_ACCESS, htonl(0x0414))) {
        perror("Unseal w1");
    } else if(i2c_smbus_write_word(MANUFACTURER_ACCESS, htonl(0x3672))) {
        perror("Unseal w2");
    } else {
        puts("write unseal data succeeded\n");
    }
}

static void try_unseal() {
    uint8_t sealed = is_sealed();
    if (sealed) {
        printf("> Device is sealed; trying to unseal\n");
        unseal();
        if (is_sealed()) {
            printf(">>> Device is still sealed :(\n");
        } else {
            printf(">>> Device seems to be unsealed now :)\n");
        }
    }
}

int main(int argc, char **argv) {
    int fd;
    uint16_t status;
    struct date manufactured;
    unsigned char manufacturer[32];
    unsigned char name[32];
    unsigned char chemistry[32];
    int c;
    char i2c_device[255];

    strcpy(i2c_device, "/dev/i2c-1");

    while (1) {
        static struct option long_options[] =
                {
                        {"device", required_argument, 0, 'd'},
                        {0, 0,                        0, 0}
                };
        int option_index = 0;

        c = getopt_long(argc, argv, "d:", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
                break;

            case 'd':
                strncpy(i2c_device, optarg, sizeof(i2c_device));
                i2c_device[254] = '\0';
                break;

            case '?':
                /* getopt_long already printed an error message. */
                break;

            default:
                abort();
        }
    }

    open_i2c_device(i2c_device, device_no);

    uint16_t device_type = i2c_smbus_manufacturer_read_word(0x0001);
    if (device_type == 0xff87) {
        fprintf(stderr, "Seems there is no device attached (read 0xff87)\n");
        exit(1);
    }
    printf("Device type: 0x%04x\n", device_type);
    if (argc > 1 && strcmp(argv[1], "unseal") == 0) {
        try_unseal();
        exit(0);
    }
    uint16_t firmware_version = i2c_smbus_manufacturer_read_word(0x0002);
    printf("Firmware version : %d.%02x\n", firmware_version / 256, firmware_version & 0xff);

    printf("Battery pack Temp   : %0.1f °C\n", bat_temp_c());
    printf("Design Voltage      : %0.1f V\n", bat_design_volts());
    printf("Current Voltage     : %0.1f V   C1: %3.1f V   C2: %3.1f V   C3: %3.1f V   C4: %3.1f V\n",
           bat_pack_volts(), bat_cell_volts(1), bat_cell_volts(2), bat_cell_volts(3),
           bat_cell_volts(4));
    printf("Actual Current      : %0.1f A\n", bat_pack_amps());
    printf("Average Current     : %0.1f A\n", bat_pack_amps_average());
    float cap_design = bat_design_capacity();
    printf("Design Capacity     : %0.3f Ah\n", cap_design);
    float cap_current = bat_capacity();
    printf("Full charge Capacity: %0.3f Ah (%d %%)\n", cap_current, (int)(100*cap_current/cap_design));
    float cap_charge = bat_capacity_remaining();
    printf("Capacity remaining  : %0.3f Ah\n", cap_charge);
    printf("Cycle count         : %d\n", bat_cycle_count());

    status = bat_status();
    if (status & 0xff00) {
        printf("ALARMS: ");
        if (status & 0x8000) printf("OVERCHARGED ");
        if (status & 0x4000) printf("TERMINATE_CHARGE ");
        if (status & 0x2000) printf("0x2000 ");
        if (status & 0x1000) printf("OVERTEMP ");
        if (status & 0x0800) printf("TERMINATE_DISCHARGE ");
        if (status & 0x0400) printf("0x0400 ");
        if (status & 0x0200) printf("REMAINING_CAPACITY ");
        if (status & 0x0100) printf("REMAINING_TIME ");
        printf("\n");
    }
    if (status & 0x00f0) {
        printf("Status: ");
        if (status & 0x0080) printf("Initialized ");
        if (status & 0x0040) printf("Discharging ");
        if (status & 0x0020) printf("Fully charged ");
        if (status & 0x0010) printf("Fully discharged ");
        printf("\n");
    }
    if (status & 0x000f) {
        printf("Error code 0x%x\n", status & 0x000f);
    }
    bat_manufactured_date(&manufactured);
    printf("Manufactured on     : %4d-%02d-%02d\n", manufactured.year, manufactured.month, manufactured.day);
    bat_manufacturer(manufacturer);
    printf("Manufacturer        : %s\n", manufacturer);
    bat_devicename(name);
    printf("Device name         : %s\n", name);
    bat_chemistry(chemistry);
    printf("Device chemistry    : %s\n", chemistry);

    //printf("Device health       : %d %%\n", i2c_smbus_read_word(0x4f));
    //printf("OperationStatus     : 0x%04x\n", i2c_smbus_read_word(0x54));
    //printf("PF Status           : 0x%04x\n", i2c_smbus_read_word(0x53));
    uint8_t sealed = is_sealed();
    if (sealed) {
        printf("> Device is sealed; trying to unseal\n");
        unseal();
        if (is_sealed()) {
            printf(">>> Device is still sealed :(\n");
        }
    }
    //uint16_t opStatus = i2c_smbus_read_word(0x54);
    uint16_t opStatus = i2c_smbus_manufacturer_read_word(0x54);
    printf("OperationStatus     : 0x%04x  ", opStatus);
    if ((opStatus & 0b0100000000000000) == 0) printf("FAS ");
    if (opStatus & 0b0010000000000000) printf("SEALED ");
    if (opStatus & 0x0020) printf("DIS-FAULT ");
    if (opStatus & 0x0010) printf("DIS-DISABLED ");
    if (opStatus & 0x0008) printf("DIS-INHIBIT ");
    if (opStatus & 0x0002) printf("VOK ");
    if (opStatus & 0x0001) printf("QEN ");
    printf("\n");

    //printf("PF Status           : 0x%04x\n", i2c_smbus_read_word(0x53));
    uint16_t mft_status = i2c_smbus_manufacturer_read_word(0x0006);
    printf("Manufacturer Status : 0x%04x  ", mft_status);
    unsigned char pfState = mft_status & 0x000f;
    char *states[16];
    states[0]= "WakeUp";
    states[1]=  "NormalDischarge";
    states[2]="n/a2";
    states[3]="Pre-Charge";
    states[4]="n/a4";
    states[5]="Charge";
    states[6]="n/a6";
    states[7]="ChargeTermination";
    states[8]="FaultChargeTerminate";
    states[9]="PermanentFailure";
    states[10]="Overcurrent";
    states[11]="Overtemperature";
    states[12]="BatteryFailure";
    states[13]="Sleep";
    states[14]="DischargeProhibited";
    states[15]="BatteryRemoved";
    char *pfailures[4] = {
            "Fuse blown", "Cell imbalance", "Safety voltage", "FET"
    };
    printf("%s", states[pfState]);
    if (pfState == 0x09) {
        unsigned char pfCause = (mft_status >> 4) & 0x03;
        printf("  PERMFAIL: %s", pfailures[pfCause]);
    }
    printf("\n");

    uint16_t soh;
    int8_t err = i2c_smbus_read_word(0x4f, &soh);
    if(err)
        printf("State of health     : error\n");
    else {
        printf("State of health     : %d %%  (%s)\n", soh & 0x00ff,
               soh & 0x0200 ? "DET-FAULT" : soh & 0x0100 ? "DET-WARN" : soh & 0x0400 ? "LIFE LIMIT" : "");
        printf("\n");
    }

    uint16_t pf_status = i2c_smbus_manufacturer_read_word(0x0053);
    printf("PERMFAIL Status     : 0x%04x  ", pf_status);
    if(pf_status && 0x2000) printf("SUV ");
    if(pf_status && 0x0800) printf("SOCD ");
    if(pf_status && 0x0400) printf("SOCC ");
    if(pf_status && 0x0200) printf("AFE_P ");
    if(pf_status && 0x0100) printf("AFE_C ");
    if(pf_status && 0x0080) printf("DFF ");
    if(pf_status && 0x0040) printf("DFETF ");
    if(pf_status && 0x0020) printf("CFETF ");
    if(pf_status && 0x0010) printf("CIM_R ");
    if(pf_status && 0x0008) printf("SOT1D ");
    if(pf_status && 0x0004) printf("SOT1C ");
    if(pf_status && 0x0002) printf("SOV ");
    if(pf_status && 0x0001) printf("PFIN ");
    printf("\n");

    //unsigned char afe_data[16];
    //printf("AFEData     : %s\n", i2c_smbus_read_cstring(0x45,afe_data, 16));

}
